package main

import (
	. "gitlab.com/gumeniukcom/fcha/src"
	"log"
	"os"
)

func main() {
	storage := &Storage{}
	mapper := &Mapping{}
	if err := ParseCSVFIle("mappings.csv", ';', mapper); err != nil {
		log.Fatal(err)
		return
	}
	pricater := &Pricater{Mapper: mapper, Storage: storage}
	if err := ParseCSVFIle("pricat.csv", ';', pricater); err != nil {
		log.Fatal(err)
		return
	}

	if err := storage.Minimize(); err != nil {
		log.Fatal(err)
		return
	}

	res, err := storage.JSON()
	if err != nil {
		log.Fatal(err)
		return
	}
	if err := os.WriteFile("output.json", res, 0666); err != nil {
		log.Fatal(err)
		return
	}
}
