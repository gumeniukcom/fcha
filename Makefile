all: fmt build

.PHONY: vendor fmt

GOFILES=`go list ./... | grep -v vendor`
PWD=$(CURDIR)

fmt:
	go fmt $(GOFILES)
build:
	go build  -mod=mod -o fcha .
vendor:
	go mod vendor
gocritic:
	gocritic check $(GOFILES)
run:
	go run main.go
test:
	go test $(GOFILES)
testv:
	go test -v $(GOFILES)
easy:
	easyjson --all src/structs/storage.go