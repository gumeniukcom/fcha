fcha
----

Dependecies
--
Golang v1.17

How to build
--
run command
```bash
    make build
```

or
```bash
    go build  -mod=mod -o fcha .
```


How to run
----
with bin file
```bash
  ./fcha
```

or
```bash
    make run
```

Where result?
----
in `output.json` file