package src

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type DummyTestStruct struct {
}

func (d DummyTestStruct) MapData(rec map[string]string) error {
	return nil
}

func (d DummyTestStruct) AddRecord(map[string]string) error {
	return nil
}

func TestPricater_ParseRecord_wrong_number_if_fields_in_pricat(t *testing.T) {
	pricater := &Pricater{
		Headers: []string{},
		Mapper:  DummyTestStruct{},
		Storage: DummyTestStruct{},
	}

	err := pricater.ParseHeaderRecord([]string{"catalog_code", "article_number"})
	assert.NoError(t, err)

	err = pricater.ParseRecord([]string{"data1", "data2"})
	assert.NoError(t, err)

	err = pricater.ParseRecord([]string{"data1", "data2", "data3"})
	assert.ErrorContains(t, err, "wrong number of fields in pricat")
}

func TestPricater_ParseRecord_no_special_headers(t *testing.T) {
	pricater := &Pricater{
		Headers: []string{},
		Mapper:  DummyTestStruct{},
		Storage: DummyTestStruct{},
	}

	err := pricater.ParseHeaderRecord([]string{"foo"})
	assert.ErrorContains(t, err, "not enough headers")
}
