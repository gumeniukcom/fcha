package src

import (
	"fmt"
	"gitlab.com/gumeniukcom/fcha/src/structs"
)

type Storage struct {
	data structs.Result
}

func (s *Storage) AddRecord(rec map[string]string) error {

	catalogCode, ok := rec["catalog_code"]
	if !ok {
		return fmt.Errorf("no catalog code")
	}
	delete(rec, "catalog_code")

	if s.data.CatalogArr == nil {
		s.data.CatalogArr = map[string]structs.Catalog{}
	}

	if _, ok := s.data.CatalogArr[catalogCode]; !ok {
		s.data.CatalogArr[catalogCode] = structs.Catalog{
			Code:     catalogCode,
			Articles: map[string]structs.Article{},
			Common:   structs.Mapped{},
		}
	}

	articleNumber, ok := rec["article_number"]
	if !ok {
		return fmt.Errorf("no article_number")
	}

	if _, ok := s.data.CatalogArr[catalogCode].Articles[articleNumber]; !ok {
		s.data.CatalogArr[catalogCode].Articles[articleNumber] = structs.Article{
			Variations: &[]structs.Variation{},
			Common:     structs.Mapped{},
		}
	}
	variation := structs.Variation(rec)

	tmp := append(*s.data.CatalogArr[catalogCode].Articles[articleNumber].Variations, variation)
	*s.data.CatalogArr[catalogCode].Articles[articleNumber].Variations = tmp

	return nil
}

func (s *Storage) Minimize() error {
	for _, catalog := range s.data.CatalogArr {
		articleCommonTmp := map[string]string{}
		articleCommonTmpInit := false

		if len(catalog.Common) > 0 {
			articleCommonTmp = catalog.Common
			articleCommonTmpInit = true
		}

		for _, article := range catalog.Articles {
			tmp := map[string]string{}
			f := false
			if len(article.Common) > 0 {
				tmp = article.Common
				f = true
			}
			for _, variation := range *article.Variations {
				if !f {
					tmp = variation
					f = true
					continue
				}
				tmp = intersection(tmp, variation)
			}
			for _, variation := range *article.Variations {
				for key := range tmp {
					delete(variation, key)
				}
			}
			for k, v := range tmp {
				article.Common[k] = v
			}

			if !articleCommonTmpInit {
				articleCommonTmp = article.Common
				articleCommonTmpInit = true
				continue
			}
			articleCommonTmp = intersection(articleCommonTmp, article.Common)
		}

		for _, article := range catalog.Articles {
			for key := range articleCommonTmp {
				delete(article.Common, key)
			}
		}
		for k, v := range articleCommonTmp {
			catalog.Common[k] = v
		}
	}

	return nil
}

func intersection(a, b map[string]string) map[string]string {
	m := make(map[string]bool)
	tmp := make(map[string]string)

	for key, _ := range a {
		m[key] = true
	}

	for key, _ := range b {
		if _, ok := m[key]; ok && a[key] == b[key] {
			tmp[key] = a[key]
		}
	}
	return tmp
}

func (s *Storage) JSON() ([]byte, error) {
	return s.data.MarshalJSON()
}
