package src

import (
	"fmt"
	"strings"
)

const sep = "|"

type Mapping struct {
	SrcDstTypeMap map[string][]string
	SrcDstMap     map[string]map[string]string
}

func (mpr *Mapping) ParseRecord(data []string) error {
	if len(data) != 4 {
		return fmt.Errorf("wrong number of fields in mapping record")
	}
	from := data[0]
	to := data[1]
	sourceType := data[2]
	destinationType := data[3]
	if mpr.SrcDstTypeMap == nil {
		mpr.SrcDstTypeMap = map[string][]string{}
	}

	if _, ok := mpr.SrcDstTypeMap[destinationType]; !ok {
		result := strings.Split(sourceType, sep)
		mpr.SrcDstTypeMap[destinationType] = result
	}

	if mpr.SrcDstMap == nil {
		mpr.SrcDstMap = map[string]map[string]string{}
	}
	if mpr.SrcDstMap[destinationType] == nil {
		mpr.SrcDstMap[destinationType] = map[string]string{}
	}

	if _, ok := mpr.SrcDstMap[destinationType][from]; !ok {
		mpr.SrcDstMap[destinationType][from] = to
	} else {
		return fmt.Errorf("duplicate source mapping")
	}

	return nil
}

func (mpr *Mapping) ParseHeaderRecord(data []string) error {
	return nil
}

func (mpr *Mapping) MapData(rec map[string]string) error {
	tmpMap := map[string]string{}
	for dstFieldName, srcFields := range mpr.SrcDstTypeMap {
		fieldValue := ""
		for _, from := range srcFields {
			val, ok := rec[from]
			if !ok {
				break
			}
			if fieldValue == "" {
				fieldValue = val
			} else {
				fieldValue = fieldValue + sep + val
			}
		}
		if dataForMapping, ok := mpr.SrcDstMap[dstFieldName]; ok {
			if mappedData, ok := dataForMapping[fieldValue]; ok {
				tmpMap[dstFieldName] = mappedData
				//  log.Printf("data mapped from %v to '%s' with value '%s'", srcFields, dstFieldName, mappedData)
			}
		}

	}
	for k, v := range tmpMap {
		rec[k] = v
	}
	return nil
}
