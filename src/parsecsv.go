package src

import (
	"encoding/csv"
	"io"
	"log"
	"os"
)

type CSVReaderCallback interface {
	ParseRecord([]string) error
	ParseHeaderRecord([]string) error
}

func ParseCSVFIle(filename string, separator rune, csvReaderCallback CSVReaderCallback) error {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(f)

	csvReader := csv.NewReader(f)
	csvReader.Comma = separator

	lineNumber := 0
	for {
		rec, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		//  fmt.Printf("%#v\n", rec)
		if lineNumber != 0 {
			if err := csvReaderCallback.ParseRecord(rec); err != nil {
				return err
			}
		} else {
			if err := csvReaderCallback.ParseHeaderRecord(rec); err != nil {
				return err
			}
		}
		lineNumber++
	}
	return nil
}
