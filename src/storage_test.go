package src

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gumeniukcom/fcha/src/structs"
	"testing"
)

func TestIntersection(t *testing.T) {
	a := map[string]string{"a": "a", "b": "b"}
	b := map[string]string{"b": "b", "c": "c"}
	res := intersection(a, b)
	assert.Equal(t, res, map[string]string{"b": "b"})
}

func TestStorage_Minimize(t *testing.T) {
	s := Storage{
		data: structs.Result{
			CatalogArr: map[string]structs.Catalog{
				"": {
					Common: map[string]string{},
					Code:   "",
					Articles: map[string]structs.Article{
						"1": {
							Common: map[string]string{
								"foo":  "bar",
								"foo1": "bar1",
							},
							Variations: &[]structs.Variation{},
						},
						"2": {
							Common: map[string]string{
								"foo2": "bar2",
								"foo1": "bar1",
							},
							Variations: &[]structs.Variation{},
						},
					},
				},
			},
		},
	}

	err := s.Minimize()
	assert.NoError(t, err)
	assert.Equal(t, structs.Result{
		CatalogArr: map[string]structs.Catalog{
			"": {
				Common: map[string]string{
					"foo1": "bar1",
				},
				Code: "",
				Articles: map[string]structs.Article{
					"1": {
						Common: map[string]string{
							"foo": "bar",
						},
						Variations: &[]structs.Variation{},
					},
					"2": {
						Common: map[string]string{
							"foo2": "bar2",
						},
						Variations: &[]structs.Variation{},
					},
				},
			},
		},
	}, s.data)

}
