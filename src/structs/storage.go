package structs

import (
	_ "github.com/mailru/easyjson"
	_ "github.com/mailru/easyjson/gen"
)

//easyjson:json
type CatalogArr map[string]Catalog

//easyjson:json
type Result struct {
	CatalogArr `json:"catalogs"`
}

//easyjson:json
type Article struct {
	Variations *[]Variation `json:"variations"`
	Common     Mapped       `json:"common"`
}

//easyjson:json
type Variation Mapped

//easyjson:json
type Mapped map[string]string

//easyjson:json
type Catalog struct {
	Common   Mapped             `json:"common"`
	Code     string             `json:"catalog"`
	Articles map[string]Article `json:"articles"`
}
