package src

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMapper_AddData_simple(t *testing.T) {
	mpr := &Mapping{}
	rec := []string{"winter", "Winter", "season", "season"}
	err := mpr.ParseRecord(rec)

	assert.Nil(t, err)
	assert.NotNil(t, mpr.SrcDstTypeMap, "empty mpr.SrcDstTypeMap")
	assert.Equal(t, mpr.SrcDstTypeMap["season"], []string{"season"}, "dst type not equals")
}

func TestMapper_AddData_complex(t *testing.T) {
	mpr := &Mapping{}
	rec := []string{"EU|36", "European size 36", "size_group_code|size_code", "size"}
	err := mpr.ParseRecord(rec)

	assert.Nil(t, err)
	assert.NotNil(t, mpr.SrcDstTypeMap, "empty mpr.SrcDstTypeMap")
	assert.Equal(t, mpr.SrcDstTypeMap["size"], []string{"size_group_code", "size_code"}, "dst type not equals")
}

func TestMapper_AddData_err_fields_count(t *testing.T) {
	mpr := &Mapping{}
	rec := []string{"EU|36", "European size 36", "size_group_code|size_code"}
	err := mpr.ParseRecord(rec)

	assert.Error(t, err, "error should not be nil")
	assert.ErrorContains(t, err, "wrong number of fields in mapping record")
}

func TestMapper_AddData_err_duplicate_source_mapping(t *testing.T) {
	mpr := &Mapping{}
	rec := []string{"EU|36", "European size 36", "size_group_code|size_code", "size"}
	err := mpr.ParseRecord(rec)
	assert.Nil(t, err)

	err = mpr.ParseRecord(rec)

	assert.Error(t, err, "error should not be nil")
	assert.ErrorContains(t, err, "duplicate source mapping")
}
