package src

import (
	"fmt"
)

type Mapper interface {
	MapData(rec map[string]string) error
}

type Storager interface {
	AddRecord(map[string]string) error
}

type Pricater struct {
	Headers []string
	Mapper  Mapper
	Storage Storager
}

func (pricat *Pricater) ParseHeaderRecord(data []string) error {
	pricat.Headers = data
	isCatalogCode := false
	isArticleNumber := false
	for _, header := range pricat.Headers {
		if header == "catalog_code" {
			isCatalogCode = true
		}
		if header == "article_number" {
			isArticleNumber = true
		}
	}
	if !(isCatalogCode && isArticleNumber) {
		return fmt.Errorf("not enough headers")
	}
	return nil
}

func (pricat *Pricater) ParseRecord(data []string) error {
	if len(data) != len(pricat.Headers) {
		return fmt.Errorf("wrong number of fields in pricat")
	}
	el := map[string]string{}
	for index, value := range data {
		el[pricat.Headers[index]] = value
	}
	if err := pricat.Mapper.MapData(el); err != nil {
		return err
	}
	return pricat.Storage.AddRecord(el)
}
